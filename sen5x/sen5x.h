#ifndef SEN5X_H
#define SEN5X_H

#include <zephyr/device.h>

#define PM_I2C_ADDRESS 0x69

struct sen5x_info
{
	char name[40];
	char serial[40];
};

struct sen5x_version
{
	uint8_t firmware_major;
	uint8_t firmware_minor;
	uint8_t firmware_debug;
	uint8_t hardware_major;
	uint8_t hardware_minor;
	uint8_t protocol_major;
	uint8_t protocol_minor;
	uint8_t padding;
};

struct sen5x_measurement
{
	uint16_t mass_concentration_pm1p0;
	uint16_t mass_concentration_pm2p5;
	uint16_t mass_concentration_pm4p0;
	uint16_t mass_concentration_pm10p0;
	int16_t ambient_humidity;
	int16_t ambient_temperature;
	int16_t voc_index;
	int16_t nox_index;
};

bool sen5x_init(const struct device *dev);

void sen5x_print_details();

void sen5x_reset();

void sen5x_get_info(struct sen5x_info *info);

void sen5x_print_info(struct sen5x_info *info);

void sen5x_get_serial(char *serial, uint8_t len);

void sen5x_get_name(char *name, uint8_t len);

void sen5x_get_version(struct sen5x_version *pmv);

void sen5x_start();

void sen5x_stop();

void sen5x_get_measurement(struct sen5x_measurement *pmm);

void sen5x_print_measurement(struct sen5x_measurement *pmm);

void sen5x_write_short(uint16_t command);

void sen5x_write(uint8_t *data, uint8_t len);

void sen5x_read(uint8_t *data, uint8_t len);

void sen5x_read_many(uint8_t *many, uint8_t len);

uint8_t sen5x_crc(const uint8_t *data, uint8_t len);

#endif
