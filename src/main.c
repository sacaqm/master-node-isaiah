#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/devicetree.h>
#include <stdio.h>
#include <string.h>
#include <sen5x/sen5x.h>
#include <lte_client/lte_client.h>
#include <modem/lte_lc.h>
#include <zephyr/net/http/client.h>
#include <stdlib.h>
#include <hal/nrf_power.h>
#include <zephyr/drivers/timer/nrf_rtc_timer.h>

#define LED0_NODE DT_ALIAS(led0)
static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED0_NODE, gpios);

#define SEN0_NODE DT_ALIAS(sen0)
static const struct gpio_dt_spec nRF_EN_SEN5x = GPIO_DT_SPEC_GET(SEN0_NODE, gpios);

#define SEN1_NODE DT_ALIAS(sen1)
static const struct gpio_dt_spec nRF_EN_3V3_DEV = GPIO_DT_SPEC_GET(SEN1_NODE, gpios);

#define SEN2_NODE DT_ALIAS(sen2)
static const struct gpio_dt_spec nRF_EN_5V = GPIO_DT_SPEC_GET(SEN2_NODE, gpios);

void reset(void)
{
	NVIC_SystemReset();
}

void blink()
{
	gpio_pin_set_dt(&led, 1);
	k_msleep(1000);
	gpio_pin_set_dt(&led, 0);
}

void setup()
{

	gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
	gpio_pin_set_dt(&led, GPIO_INT_LEVEL_LOW);

	gpio_pin_configure_dt(&nRF_EN_SEN5x, GPIO_OUTPUT_ACTIVE);
	gpio_pin_set_dt(&nRF_EN_SEN5x, GPIO_INT_LEVEL_HIGH);

	gpio_pin_configure_dt(&nRF_EN_3V3_DEV, GPIO_OUTPUT_ACTIVE);
	gpio_pin_set_dt(&nRF_EN_3V3_DEV, GPIO_INT_LEVEL_HIGH);

	gpio_pin_configure_dt(&nRF_EN_5V, GPIO_OUTPUT_ACTIVE);
	gpio_pin_set_dt(&nRF_EN_5V, GPIO_INT_LEVEL_HIGH);
}

void enableSen50()
{
	// Enable nRF_EN_3V3_DEV,  nRF_EN_5V,  nRF_EN_SEN5x.
	gpio_pin_set_dt(&nRF_EN_SEN5x, 0);
	gpio_pin_set_dt(&nRF_EN_3V3_DEV, 0);
	gpio_pin_set_dt(&nRF_EN_5V, 0);
}

void disableSen50()
{
	// Enable nRF_EN_3V3_DEV,  nRF_EN_5V,  nRF_EN_SEN5x.
	gpio_pin_set_dt(&nRF_EN_SEN5x, 1);
	gpio_pin_set_dt(&nRF_EN_3V3_DEV, 1);
	gpio_pin_set_dt(&nRF_EN_5V, 1);
}

// Forward declaration of the timer handler
void timer_handler(struct k_timer *dummy);

// Define the timer using the handler
K_TIMER_DEFINE(my_timer, timer_handler, NULL);

// Implementation of the timer handler
void timer_handler(struct k_timer *dummy)
{
	printk("Timer fired");
	reset();
}

int main(void)
{

	// Start the timer to fire every 1000 milliseconds (1 second)
	k_timer_start(&my_timer, K_MSEC(1800000), K_MSEC(1800000));

	printf("Welcome to IOTPM\n");

	printf("Init LED\n");
	device_is_ready(led.port);
	setup();
	blink();

	printf("Enable SEN50\n");
	enableSen50();
	blink();

	printf("Init device\n");
	sen5x_init(DEVICE_DT_GET(DT_NODELABEL(i2c2)));
	blink();

	printf("Reset device\n");
	sen5x_reset();
	blink();

	printf("Get serial number\n");
	char serial[32];
	sen5x_get_serial(serial, sizeof(serial));
	printf("serial: %s\n", serial);
	blink();

	printf("Get name\n");
	char name[32];
	sen5x_get_name(name, 32);
	printf("name: %s\n", name);
	blink();

	printf("Get version\n");
	struct sen5x_version pmv;
	sen5x_get_version(&pmv);
	printf("FW: %u.%u.%u\n", pmv.firmware_major, pmv.firmware_minor, pmv.firmware_debug);
	printf("HW: %u.%u\n", pmv.hardware_major, pmv.hardware_minor);
	printf("PR: %u.%u\n", pmv.protocol_major, pmv.protocol_minor);
	blink();
	int err;

	err = nrf_modem_lib_init();
	if (err)
	{
		printk("Modem initialization failed, err %d\n", err);
		return 0;
	}

	printk("Connect to the LTE\n");
	if (!lte_client_connect())
	{
		struct lte_client_system_mode mode;
		mode.lte_m = 0;
		mode.nb_iot = 1;
		mode.gnss = 1;
		mode.lte_p = 0;
		lte_client_set_system_mode(&mode);
		lte_client_connect();
	}

	printk("Get the network information\n");
	struct lte_client_net_info net;
	lte_client_init_net_info(&net);
	lte_client_get_net_info(&net);
	lte_client_print_net_info(&net);

	// printk("Get the gps coordinates\n");
	// struct lte_client_gps_info gps;
	// lte_client_init_gps_info(&gps);
	// lte_client_get_gps_info(&gps);

	struct sen5x_measurement pmm;

	while (true)
	{

		k_sleep(K_SECONDS(10));

		printk("Start measurement\n");
		sen5x_start();

		k_msleep(1 * 6000);

		sen5x_get_measurement(&pmm);
		sen5x_print_measurement(&pmm);

		char payload[300];
		char *str = payload;
		str += sprintf(str, "&sensorId=%15s", net.imei);
		str += sprintf(str, "&temperature=%i.%02i", (pmm.ambient_temperature / 200), abs(pmm.ambient_temperature % 200) >> 1);
		str += sprintf(str, "&humidity=%i.%02i", (pmm.ambient_humidity / 100), (pmm.ambient_humidity % 100));
		str += sprintf(str, "&pm1p0=%i.%02i", (pmm.mass_concentration_pm1p0 / 10), (pmm.mass_concentration_pm1p0 % 10));
		str += sprintf(str, "&pm2p5=%i.%02i", (pmm.mass_concentration_pm2p5 / 10), (pmm.mass_concentration_pm2p5 % 10));
		str += sprintf(str, "&pm4p0=%i.%02i", (pmm.mass_concentration_pm4p0 / 10), (pmm.mass_concentration_pm4p0 % 10));
		str += sprintf(str, "&pm10p0=%i.%02i", (pmm.mass_concentration_pm10p0 / 10), (pmm.mass_concentration_pm10p0 % 10));
		str += sprintf(str, "&voc=%i.%02i", (pmm.voc_index / 10), abs(pmm.voc_index % 10));
		str += sprintf(str, "&nox=%i.%02i", (pmm.nox_index / 10), abs(pmm.nox_index % 10));

		// printk("payload: %s\n", payload);

		// lte_client_post("https", "sacaqm.onrender.com", "443", "/api/sensors", payload);
		// lte_client_post("https", "try-again-test-isaiah.app.cern.ch", "443", "/api/sensors", "temp=25.5");

		lte_client_post("try-again-test-isaiah.app.cern.ch", "80", "/api/sensors", payload);

		printk("Stop measurement\n");
		// sen5x_stop();

		// 	// if (!gps.valid)
		// 	// {
		// 	// 	lte_client_get_gps_info(&gps);
		// 	// }
		// 	// else
		// 	// {
		// 	// 	printk("Sleep\n");
		// 	// 	k_msleep(4 * 60000);
		// 	// }
		// 	blink();
	}

	return 0;
}
