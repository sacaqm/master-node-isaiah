#include <zephyr/drivers/i2c.h>
#include <sen5x/sen5x.h>

const struct device *sen5x_dev;

bool sen5x_init(const struct device *dev)
{
	sen5x_dev = dev;
	if (!device_is_ready(sen5x_dev))
	{
		printf("SEN5X: I2C device not ready\n");
		return false;
	}
	uint32_t i2c_cfg = I2C_SPEED_SET(I2C_SPEED_STANDARD) | I2C_MODE_CONTROLLER;
	int32_t ret = i2c_configure(sen5x_dev, i2c_cfg);
	if (ret)
	{
		printf("SEN5X: Error configuring I2C\n");
		return false;
	}
	return true;
}

void sen5x_write_short(uint16_t command)
{
	uint8_t data[2];
	data[0] = (command & 0xFF00) >> 8;
	data[1] = (command & 0x00FF) >> 0;
	printf("SEN5X: write: 0x%02x%02x\n", data[0], data[1]);
	i2c_write(sen5x_dev, data, 2, 0x69);
}

void sen5x_write(uint8_t *data, uint8_t len)
{
	int32_t ret = i2c_write(sen5x_dev, data, len, PM_I2C_ADDRESS);
	if (ret)
	{
		printf("SEN5X: Error i2c write\n");
	}
}

void sen5x_read(uint8_t *data, uint8_t len)
{
	int32_t ret = i2c_read(sen5x_dev, data, len, PM_I2C_ADDRESS);
	if (ret)
	{
		printf("SEN5X: Error i2c read\n");
	}
}

void sen5x_read_many(uint8_t *many, uint8_t len)
{
	uint8_t data[100];
	uint8_t mlen = len + (len / 2);
	sen5x_read((uint8_t *)data, mlen);
	// printf("SEN5X: many (%i)",mlen);
	// for(uint32_t i=0;i<mlen;i++){printf("%02x",(uint32_t)data[i]);}
	// printf("\n");
	for (uint32_t i = 0; i < len / 2; i++)
	{
		many[2 * i] = data[3 * i];
		many[2 * i + 1] = data[3 * i + 1];
		// if(many[2*i+2]!=sen5x_crc(&many[2*i],2)){printf("crc error: %i\n",(2*i));}
	}
}

uint8_t sen5x_crc(const uint8_t *data, uint8_t len)
{
	uint8_t crc = 0xFF;
	for (uint32_t current_byte = 0; current_byte < len; ++current_byte)
	{
		crc ^= (data[current_byte]);
		for (uint8_t crc_bit = 8; crc_bit > 0; --crc_bit)
		{
			if (crc & 0x80)
				crc = (crc << 1) ^ 0x31;
			else
				crc = (crc << 1);
		}
	}
	return crc;
}

void sen5x_reset()
{
	sen5x_write_short(0xD304);
	k_msleep(100);
}

void sen5x_get_serial(char *serial, uint8_t len)
{
	sen5x_write_short(0xD033);
	for (uint32_t i = 0; i < len; i++)
	{
		serial[i] = 0;
	}
	k_msleep(50);
	sen5x_read_many(serial, len);
}

void sen5x_get_name(char *name, uint8_t len)
{
	sen5x_write_short(0xD014);
	for (uint32_t i = 0; i < len; i++)
	{
		name[i] = 0;
	}
	k_msleep(50);
	sen5x_read_many(name, len);
}

void sen5x_get_version(struct sen5x_version *pmv)
{
	sen5x_write_short(0xD100);
	k_msleep(50);
	sen5x_read_many((uint8_t *)pmv, 8);
}

void sen5x_start()
{
	sen5x_write_short(0x0021);
	k_msleep(50);
}

void sen5x_stop()
{
	sen5x_write_short(0x0104);
	k_msleep(50);
}

void sen5x_get_measurement(struct sen5x_measurement *pmm)
{
	sen5x_write_short(0x03C4);
	k_msleep(20);
	uint8_t data[16];
	sen5x_read_many(&data[0], 16);
	pmm->mass_concentration_pm1p0 = data[0] << 8 | data[1];
	pmm->mass_concentration_pm2p5 = data[2] << 8 | data[3];
	pmm->mass_concentration_pm4p0 = data[4] << 8 | data[5];
	pmm->mass_concentration_pm10p0 = data[6] << 8 | data[7];
	pmm->ambient_humidity = data[8] << 8 | data[9];
	pmm->ambient_temperature = data[10] << 8 | data[11];
	pmm->voc_index = data[12] << 8 | data[13];
	pmm->nox_index = data[14] << 8 | data[15];
}

void sen5x_print_measurement(struct sen5x_measurement *pmm)
{
	printf("Mass concentration pm1p0: %i ug/m3\n", (int32_t)(pmm->mass_concentration_pm1p0 / 10.0f));
	printf("Mass concentration pm2p5: %i ug/m3\n", (int32_t)(pmm->mass_concentration_pm2p5 / 10.0f));
	printf("Mass concentration pm4p0: %i ug/m3\n", (int32_t)(pmm->mass_concentration_pm4p0 / 10.0f));
	printf("Mass concentration pm10p0: %i ug/m3\n", (int32_t)(pmm->mass_concentration_pm10p0 / 10.0f));
	printf("Ambient humidity: %i %%RH\n", (int32_t)(pmm->ambient_humidity / 100.0f));
	printf("Ambient temperature: %i C\n", (int32_t)(pmm->ambient_temperature / 200.0f));
	printf("Voc index: %i\n", (int32_t)(pmm->voc_index / 10.0f));
	printf("Nox index: %i\n", (int32_t)(pmm->nox_index / 10.0f));
}

void sen5x_get_info(struct sen5x_info *info)
{
	sen5x_get_name(info->name, 32);
	sen5x_get_serial(info->serial, 32);
}

void sen5x_print_info(struct sen5x_info *info)
{
	printf("SEN5X: Device name: %s\n", info->name);
	printf("SEN5X: Serial number: %s\n", info->serial);
}

void sen5x_print_details()
{
	char name[32];
	sen5x_get_name(name, 32);
	printf("SEN5X: Device name: %s\n", name);

	char serial[32];
	sen5x_get_serial(serial, sizeof(serial));
	printf("SEN5X: Serial number: %s\n", serial);

	struct sen5x_version pmv;
	sen5x_get_version(&pmv);
	printf("SEN5X: FW: %u.%u.%u\n", pmv.firmware_major, pmv.firmware_minor, pmv.firmware_debug);
	printf("SEN5X: HW: %u.%u\n", pmv.hardware_major, pmv.hardware_minor);
	printf("SEN5X: PR: %u.%u\n", pmv.protocol_major, pmv.protocol_minor);
}
